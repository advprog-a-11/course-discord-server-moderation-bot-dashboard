package com.bot.bot.exception;

public class BotUnverifiedCommandException extends Exception {
    public BotUnverifiedCommandException() {
    }

    public BotUnverifiedCommandException(String errorMessage) {
        super(errorMessage);
    }
}
