package com.bot.bot.model;

import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.entities.User;


@Entity
@Table(name = "bot_user")
@Data
@NoArgsConstructor
public class BotUser {

    @Id
    @Column(name = "id", updatable = false)
    private Long userId;

    @Column(name = "name")
    private String name;


    @Column(name = "avatar_url")
    private String avatarUrl;

    public BotUser(User author) {
        this.userId = author.getIdLong();
        this.name = author.getName();
        this.avatarUrl = author.getEffectiveAvatarUrl();
    }

    @Override
    public String toString() {
        return String.format("UserID: %d%nname: %s%n%navatarUrl: %s",
            userId, name, avatarUrl);
    }


}
