package com.bot.bot.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bot.bot.exception.BotUnverifiedCommandException;
import com.bot.bot.model.BotUser;
import com.bot.bot.repository.BotUserRepository;

import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BotUtilityImpl implements BotUtility {

    @Autowired
    BotUserRepository botUserRepository;

    @Override
    public boolean isQuery(String command, String query) {
        return command.trim().toLowerCase().startsWith("!" + query);
    }

    @Override
    public List<String> verify(User author, String command, int args)
        throws BotUnverifiedCommandException {
        List<String> splitted = new ArrayList<>(Arrays.asList(command.trim().split("\\s*,\\s*")));
        if (splitted.size() != args + 1) {
            throw new BotUnverifiedCommandException(
                "Arguments are not enough " + author.getAsMention());
        }
        splitted.remove(0);

        if (botUserRepository.findByUserId(author.getIdLong()) == null) {
            BotUser botUser = new BotUser(author);
            botUserRepository.save(botUser);
        }

        return splitted;
    }
}
