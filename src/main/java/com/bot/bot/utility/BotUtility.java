package com.bot.bot.utility;

import java.util.List;

import com.bot.bot.exception.BotUnverifiedCommandException;

import net.dv8tion.jda.api.entities.User;
import org.springframework.stereotype.Service;

@Service
public interface BotUtility {
    boolean isQuery(String command, String query);

    List<String> verify(User author, String command, int args)
        throws BotUnverifiedCommandException;
}
