package com.bot.bot.feature.pingpong;

import java.time.Duration;
import java.time.OffsetDateTime;

import com.bot.bot.utility.BotUtility;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class PingPongListener extends ListenerAdapter {

    @Autowired
    BotUtility botUtility;

    public void pongAccepted(Message responseMessage) {
        long time =
            Math.abs(Duration.between(responseMessage.getTimeCreated(), OffsetDateTime.now())
                .toMillis());
                responseMessage.getChannel().sendMessage(
            "Pong! " + time + "ms").queue();
        
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();
        if (botUtility.isQuery(message.getContentRaw(), "ping")) {
            MessageChannel channel = event.getChannel();
            channel.sendMessage("Pong!")
                .queue(this::pongAccepted);
        }
    }

}
