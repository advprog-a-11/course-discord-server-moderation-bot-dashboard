package com.bot.bot.feature.help;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import com.bot.bot.utility.BotUtility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class HelpListener extends ListenerAdapter {

    private final MessageEmbed helpMessageEmbed;

    @Autowired
    private BotUtility botUtility;

    public HelpListener() {
        EmbedBuilder embedBuilder = new EmbedBuilder();

        embedBuilder.setDescription("**Help:**");

        embedBuilder.addField("**Role Management**",
            "'!role -create <role>': --------- \n"
            + "`!role`: ----- .\n"
            + "", false);
        embedBuilder.addField("**Category/Channel Management**",
            "`!category <category>`: category.\n"
            + " ", false);

        embedBuilder.addField("**Thread Management**",
            "`!thread <thread>`: thread.\n"
            + " ", false);

        embedBuilder.addField("**Ban/Warn User**",
            "`!ban -user <@UserName> <Your reason here>`: ban user.\n"
            + "`!ban -list`: ban List.", false);

        helpMessageEmbed = embedBuilder.build();
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message message = event.getMessage();
        if (botUtility.isQuery(message.getContentRaw(), "help")) {
            MessageChannel channel = event.getChannel();
            channel.sendMessage(helpMessageEmbed).queue();
        }
    }

}
