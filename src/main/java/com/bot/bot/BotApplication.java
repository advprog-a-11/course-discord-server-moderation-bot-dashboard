package com.bot.bot;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

import com.bot.bot.service.BotService;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableConfigurationProperties
@EnableAdminServer
@EnableAsync
@EnableScheduling
public class BotApplication implements CommandLineRunner {

    private final BotService botService;

    public BotApplication(BotService botService) {
        this.botService = botService;
    }

    public static void main(String[] args) {
        SpringApplication.run(BotApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        botService.startBot();
    }
}
