package com.bot.bot.service;

import javax.security.auth.login.LoginException;
import net.dv8tion.jda.api.JDA;

public interface BotService {
    void startBot() throws InterruptedException, LoginException;

    JDA getjda();
}
