package com.bot.bot.service;

import javax.security.auth.login.LoginException;

import com.bot.bot.feature.help.HelpListener;
import com.bot.bot.feature.pingpong.PingPongListener;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BotServiceImpl implements BotService {

    @Value("${jda.discord.token}")
    private String botToken;
    private JDA jda;

    @Autowired
    private PingPongListener pingPongListener;

    @Autowired
    private HelpListener helpListener;

    @Override
    public void startBot() throws InterruptedException, LoginException {
        jda = JDABuilder.createDefault(botToken).build();
        jda.addEventListener(pingPongListener);
        jda.addEventListener(helpListener);
        jda.awaitReady();
    }

    @Override
    public JDA getjda() {
        return jda;
    }
}