package com.bot.bot.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DashboardWebController {

    @GetMapping("")
    public String landingPage() {
        return "web/index";
    }
    
}
