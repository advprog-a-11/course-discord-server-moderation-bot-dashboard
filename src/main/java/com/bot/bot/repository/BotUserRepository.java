package com.bot.bot.repository;

import com.bot.bot.model.BotUser;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BotUserRepository extends JpaRepository<BotUser, Integer> {
    BotUser findByUserId(long id);
}
